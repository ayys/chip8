all: build

SRCS  = src/main.c\
src/ram.c     src/registers.c\
src/stack.c   src/instructions.c\
src/parse.c   src/opcodes.c\
src/display.c src/keyboard.c

CC = gcc
FLIPBITS_OPTS = -g
OPTS = -g `sdl2-config --libs --cflags`

DEBUG=

makebin:
	mkdir -p bin

flipbits: src/flipbits.c
	$(CC) $(FLIPBITS_OPTS) $(DEBUG) -o bin/$@ $^

chip8: $(SRCS) 
	$(CC) $(OPTS) $(DEBUG) $^ -o bin/$@

build: makebin flipbits chip8

run: build
	./bin/chip8 demo

clean:
	rm bin/chip8 || true
