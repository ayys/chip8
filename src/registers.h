#include <stdint.h>


#ifndef __REGISTERS
#define __REGISTERS
/* 
   16 general purpose registers
 */
extern uint8_t gp_rs[];

/* 
   2 special registers for delay and sound timers
 */
struct _sp_rs {
  uint8_t dt;
  uint8_t st;
};

extern struct _sp_rs sp_rs;
/* Program Counter */
extern uint16_t pc;

/* Stack Pinter */
extern uint8_t sp;

/* 
   There is a 16-bit register called I.
   This register is generally used to store memory addresses, 
   so only the lowest (rightmost) 12 bits are usually used.
*/
uint16_t i;


extern void initialize_registers();


#endif
