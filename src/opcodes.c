#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <stdlib.h>

#include "opcodes.h"
#include "display.h"
#include "instructions.h"
#include "keyboard.h"
#include "stack.h"
#include "registers.h"
#include "ram.h"
#include "parse.h"

bool pause_execution = false;

void opcode_execute_at_loc(SDL_Window *window, SDL_Renderer *renderer, uint16_t pc) {

  uint16_t ins = ins_from_ram(pc);
  uint8_t opcode = parse_ins(ins);
  switch (opcode) {
  case INS_0NNN:
    _0nnn(window, renderer, ins);
    break;
  case INS_00E0:
    _00e0(window, renderer, ins);
    break;
  case INS_00EE:
    _00ee(window, renderer, ins);
    break;
  case INS_1NNN:
    _1nnn(window, renderer, ins);
    break;
  case INS_2NNN:
    _2nnn(window, renderer, ins);
    break;
  case INS_3XNN:
    _3xnn(window, renderer, ins);
    break;
  case INS_4XNN:
    _4xnn(window, renderer, ins);
    break;
  case INS_5XY0:
    _5xy0(window, renderer, ins);
    break;
  case INS_6XNN:
    _6xnn(window, renderer, ins);
    break;
  case INS_7XNN:
    _7xnn(window, renderer, ins);
    break;
  case INS_8XY0:
    _8xy0(window, renderer, ins);
    break;
  case INS_8XY1:
    _8xy1(window, renderer, ins);
    break;
  case INS_8XY2:
    _8xy2(window, renderer, ins);
    break;
  case INS_8XY3:
    _8xy3(window, renderer, ins);
    break;
  case INS_8XY4:
    _8xy4(window, renderer, ins);
    break;
  case INS_8XY5:
    _8xy5(window, renderer, ins);
    break;
  case INS_8XY6:
    _8xy6(window, renderer, ins);
    break;
  case INS_8XY7:
    _8xy7(window, renderer, ins);
    break;
  case INS_8XYE:
    _8xye(window, renderer, ins);
    break;
  case INS_9XY0:
    _9xy0(window, renderer, ins);
    break;
  case INS_ANNN:
    _annn(window, renderer, ins);
    break;
  case INS_BNNN:
    _bnnn(window, renderer, ins);
    break;
  case INS_CXNN:
    _cxnn(window, renderer, ins);
    break;
  case INS_DXYN:
    _dxyn(window, renderer, ins);
    break;
  case INS_EX9E:
    _ex9e(window, renderer, ins);
    break;
  case INS_EXA1:
    _exa1(window, renderer, ins);
    break;
  case INS_FX07:
    _fx07(window, renderer, ins);
    break;
  case INS_FX0A:
    _fx0a(window, renderer, ins);
    break;
  case INS_FX15:
    _fx15(window, renderer, ins);
    break;
  case INS_FX18:
    _fx18(window, renderer, ins);
    break;
  case INS_FX1E:
    _fx1e(window, renderer, ins);
    break;
  case INS_FX29:
    _fx29(window, renderer, ins);
    break;
  case INS_FX33:
    _fx33(window, renderer, ins);
    break;
  case INS_FX55:
    _fx55(window, renderer, ins);
    break;
  case INS_FX65:
    _fx65(window, renderer, ins);
    break;
  }
}

/* IGNORE THIS INSTRUCTION */
void _0nnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
}

void _00e0(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  initialize_display();
}
void _00ee(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  pc = stack[sp];
  sp--;
}
void _1nnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  pc = ins_addr(ins);
}
void _2nnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  stack[++sp] = pc;
  pc = ins_addr(ins);
}
void _3xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  if ( gp_rs[ins_hex(ins, 1)] == ins_lsbyte(ins) ) pc += 2;
}
void _4xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  if ( !(gp_rs[ins_hex(ins, 1)] == ins_lsbyte(ins)) ) pc += 2;
}
void _5xy0(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
   if ( gp_rs[ins_hex(ins, 1)] == gp_rs[ins_hex(ins, 2)] ) pc += 2;
}
void _6xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] = ins_lsbyte(ins);
 }
void _7xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] += ins_lsbyte(ins);
}
void _8xy0 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] = gp_rs[ins_hex(ins, 2)];
}
void _8xy1 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] |= gp_rs[ins_hex(ins, 2)];
}
void _8xy2 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] &= gp_rs[ins_hex(ins, 2)];
}
void _8xy3 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] ^= gp_rs[ins_hex(ins, 2)];
}
void _8xy4 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint16_t sum = gp_rs[ins_hex(ins, 1)] + gp_rs[ins_hex(ins, 2)];
  gp_rs[15] = (sum > 255) ? 1 : 0;
  gp_rs[ins_hex(ins, 1)] = sum;
}
void _8xy5 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  uint8_t vy = gp_rs[ins_hex(ins, 2)];
  gp_rs[15] = (vx > vy) ? 1 : 0;
  gp_rs[ins_hex(ins, 1)] = vy - vx;
}
void _8xy6 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  gp_rs[15] = ( (vx & 0x01) != 0 ) ? 1 : 0;
  gp_rs[ins_hex(ins, 1)] = vx / 2;
}
void _8xy7 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  uint8_t vy = gp_rs[ins_hex(ins, 2)];
  gp_rs[15] = (vy > vx) ? 1 : 0;
  gp_rs[ins_hex(ins, 1)] = vy - vx;
}
void _8xye (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  gp_rs[15] = ( vx & 1 ) ? 1 : 0;
  gp_rs[ins_hex(ins, 1)] *= 2;
}
void _9xy0 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  if ( gp_rs[ins_hex(ins, 1)] != gp_rs[ins_hex(ins, 2)] ) pc += 2;
}
void _annn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  i = ins_addr(ins);
}
void _bnnn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  pc = ins_addr(ins) + gp_rs[0];
}
void _cxnn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  srand(time(NULL));
  uint8_t random_int = rand() % 128;
  gp_rs[ ins_hex(ins, 1) ] = random_int + ins_lsbyte(ins);
}
void _dxyn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  uint8_t vy = gp_rs[ins_hex(ins, 2)];
  bool collision = draw_sprite(i, ins_hex(ins, 3), vx, vy);
  gp_rs[15] = collision;
}
void _ex9e (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  if (keyboard_get(gp_rs[ins_hex(ins, 1)]) == true) pc += 2;
}
void _exa1 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  if (keyboard_get(gp_rs[ins_hex(ins, 1)]) == false) pc += 2;
}
void _fx07 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  gp_rs[ins_hex(ins, 1)] = sp_rs.dt;
}
void _fx0a (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  if (keyboard != K_NO_KEYS){
    pause_execution = false;
    gp_rs[ins_hex(ins, 1)] = keyboard;
  } else pause_execution = true;
}
void _fx15 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  sp_rs.dt = gp_rs[ins_hex(ins, 1)];
}
void _fx18 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  sp_rs.st = gp_rs[ins_hex(ins, 1)];
}
void _fx1e (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  i += gp_rs[ins_hex(ins, 1)];
}
void _fx29 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  i = vx * 5;
}
void _fx33 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  uint8_t vx = gp_rs[ins_hex(ins, 1)];
  ram[i] = vx / 100;
  ram[i + 1] = (vx / 10) % 10;
  ram[i + 2] = vx % 10;
}
void _fx55 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  for (uint8_t index = 0; index < ins_hex(ins, 1); index++ )
    ram[i + index] = gp_rs[index];
}
void _fx65 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins){
  for (uint8_t index = 0; index < ins_hex(ins, 1); index++ )
    gp_rs[index] = ram[i + index];
}
