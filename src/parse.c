#include "opcodes.h"
#include "instructions.h"
#include "parse.h"

#include <SDL2/SDL.h>

static uint8_t match_for_0(uint16_t ins) {
  if (ins == 0xe000) {
    
    return INS_00E0;
  }
  else if (ins == 0xee00) {

    return INS_00EE;
  }

  return INS_0NNN;
}

static uint8_t match_for_1(uint16_t ins) {

  return INS_1NNN;
}

static uint8_t match_for_2(uint16_t ins) {

  return INS_2NNN;
}

static uint8_t match_for_3(uint16_t ins) {

  return INS_3XNN;
}

static uint8_t match_for_4(uint16_t ins) {

  return INS_4XNN;
}

static uint8_t match_for_5(uint16_t ins) {

  return INS_5XY0;
}

static uint8_t match_for_6(uint16_t ins) {

  return INS_6XNN;
}

static uint8_t match_for_7(uint16_t ins) {

  return INS_7XNN;
}

static uint8_t match_for_8(uint16_t ins) {
  if (ins_hex(ins, 3) == 0) {

    return INS_8XY0;
  }
  else if (ins_hex(ins, 3) == 1) {

    return INS_8XY1;
  }
  else if (ins_hex(ins, 3) == 2) {

    return INS_8XY2;
  }
  else if (ins_hex(ins, 3) == 3) {

    return INS_8XY3;
  }
  else if (ins_hex(ins, 3) == 4) {

    return INS_8XY4;
  }
  else if (ins_hex(ins, 3) == 5) {

    return INS_8XY5;
  }
  else if (ins_hex(ins, 3) == 6) {

    return INS_8XY6;
  }
  else if (ins_hex(ins, 3) == 7) {

    return INS_8XY7;
  }
  else if (ins_hex(ins, 3) == 0x0E) {

    return INS_8XYE;
  }
  return 0;
}

static uint8_t match_for_9(uint16_t ins) {
  if (ins_hex(ins, 3) == 0) return INS_9XY0;
  return 0;
}

static uint8_t match_for_A(uint16_t ins) {

  return INS_ANNN;
}

static uint8_t match_for_B(uint16_t ins) {

  return INS_BNNN;
}

static uint8_t match_for_C(uint16_t ins) {

  return INS_CXNN;
}

static uint8_t match_for_D(uint16_t ins) {

  return INS_DXYN;
}

static uint8_t match_for_E(uint16_t ins) {
  if (ins_lsbyte(ins) == 0x9E) {

    return INS_EX9E;
  }
  else if (ins_lsbyte(ins) == 0xA1) {

    return INS_EXA1;
  }
  return 0;
}

static uint8_t match_for_F(uint16_t ins) {
  if (ins_lsbyte(ins) == 0x07) return INS_FX07;
  else if (ins_lsbyte(ins) == 0x0A) {
    return INS_FX0A;
  }
  else if (ins_lsbyte(ins) == 0x15) {
    return INS_FX15;
  }
  else if (ins_lsbyte(ins) == 0x18) {
    return INS_FX18;
  }
  else if (ins_lsbyte(ins) == 0x1E) {
    return INS_FX1E;
  }
  else if (ins_lsbyte(ins) == 0x29) {
    return INS_FX29;
  }
  else if (ins_lsbyte(ins) == 0x33) {
    return INS_FX33;
  }
  else if (ins_lsbyte(ins) == 0x55) {
    return INS_FX55;
  }
  else if (ins_lsbyte(ins) == 0x65) {
    return INS_FX65;
  }
  return 0;
}



uint8_t parse_ins(uint16_t ins) {
  /* 
     check the first hex of instruction and
     match it with all values from 0-F in hex
  */
  if(ins == 0) return 0;
  switch(ins_hex(ins, 0)) {
  case 0x00:
    return match_for_0(ins);
  case 0x01:
    return match_for_1(ins);
  case 0x02:
    return match_for_2(ins);
  case 0x03:
    return match_for_3(ins);
  case 0x04:
    return match_for_4(ins);
  case 0x05:
    return match_for_5(ins);
  case 0x06:
    return match_for_6(ins);
  case 0x07:
    return match_for_7(ins);
  case 0x08:
    return match_for_8(ins);
  case 0x09:
    return match_for_9(ins);
  case 0x0A:
    return match_for_A(ins);
  case 0x0B:
    return match_for_B(ins);
  case 0x0C:
    return match_for_C(ins);
  case 0x0D:
    return match_for_D(ins);
  case 0x0E:
    return match_for_E(ins);
  case 0x0F:
    return match_for_F(ins);
  }
  return 0;
}
