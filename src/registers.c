#include <stdint.h>
#include <string.h>

#include "registers.h"
/* 
   16 general purpose registers
 */
uint8_t gp_rs[16];

/* 
   There is a 16-bit register called I.
   This register is generally used to store memory addresses, 
   so only the lowest (rightmost) 12 bits are usually used.
*/
uint16_t i;

/* 
   2 special registers for delay and sound timers
*/

struct _sp_rs sp_rs;
/* Program Counter */
uint16_t pc = 510;		/* it will be incremented before an instruction  */

/* Stack Pinter */
uint8_t sp = 0;

void initialize_registers() {
  memset(gp_rs, 0, 16);
  sp_rs.dt = 0;
  sp_rs.st = 0;
}
