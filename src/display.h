#include <stdbool.h>
#include <SDL2/SDL.h>

#define DISPLAY_WIDTH 64
#define DISPLAY_HEIGHT 32

/* 
   64 - width of screen, 32 - height of screen 
*/

extern void initialize_display();
extern bool PIXELS[64][32];
extern SDL_Rect get_pixel_rect(SDL_Window *window, uint8_t x, uint8_t y);
extern int draw_screen(SDL_Window *window, SDL_Renderer *renderer);
extern int draw_pixel(SDL_Window *window, SDL_Renderer *renderer, uint8_t x, uint8_t y);
extern bool draw_sprite(uint16_t start, uint8_t n,
			uint8_t x, uint8_t y);
extern bool draw_sprite_byte(uint8_t byte, uint8_t x, uint8_t y);

extern bool get_pixel(uint8_t x, uint8_t y);
extern bool insert_pixel(uint8_t x, uint8_t y);
extern bool remove_pixel(uint8_t x, uint8_t y);;
