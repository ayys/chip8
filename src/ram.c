#include "ram.h"
#include "font.h"
#include "instructions.h"
#include <string.h>

uint8_t ram[RAM_SIZE];
uint16_t program_length = 0;

void initialize_ram() {
  memset( ram, 0, RAM_SIZE);
  /* load font to ram */
  memcpy(ram, font, 16 * 5);
}

void load_program_to_ram(FILE* f){
  int c;
  size_t pos = 512;
  while ((c = fgetc(f)) != EOF) {
    ram[pos] = c;
    pos += 1;
  }
  program_length = pos;
}
