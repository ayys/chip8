#include <stdint.h>
#include <string.h>

#include "stack.h"

uint16_t stack[STACK_SIZE];

void initialize_stack() {
  memset(stack, 0, STACK_SIZE);
}

