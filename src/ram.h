#include <stdint.h>
#include <stdio.h>

#define RAM_SIZE (4*1024)
extern uint8_t ram[];

extern void initialize_ram();
extern void load_program_to_ram(FILE* f);

extern uint16_t program_length;
