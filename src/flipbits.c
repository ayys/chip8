#include <stdio.h>
#include <stdint.h>


/* 

USAGE - 

flipbits <input file> [<output file>]

Flip bytes from input file and write to output file.

Example:
Assume a file with 4 bytes - 1F 2E 6D 8C

INPUT = 1F 2E 6D 8C
OUTPUT = 2E 1F 8C 6D

If the input file has odd number of bytes, output is undefined.
 
If output file is not specified, write to stdout.
If input file is not specified, read from stdin.
 */


struct ins {
  uint8_t b1;
  uint8_t b2;
};

int main(int argc, char **argv) {
  FILE *fin;
  if (argc <= 1) fin = stdin;
  else fin = fopen(argv[1], "r");
  FILE *fout;
  if (argc == 2)
    fout = stdout;
  else fout = fopen(argv[2], "w");
  struct ins c = {0, 0};
  struct ins d = {0, 0};
  while (fread(&c, 1, 2, fin) != 0) {
    d.b1 = c.b2; d.b2 = c.b1;
    fwrite( &d, 1, 2, fout );
  }
  return 0;
}
