#include <stdint.h>
#include <stdbool.h>

#define K_0 1
#define K_1 2
#define K_2 3
#define K_3 4
#define K_4 5
#define K_5 6
#define K_6 7
#define K_7 8
#define K_8 9
#define K_9 10
#define K_A 11
#define K_B 12
#define K_C 13
#define K_D 14
#define K_E 15
#define K_F 16
#define K_NO_KEYS 17


extern uint8_t keyboard;

extern bool keyboard_get(uint8_t key);
extern void keyboard_set(uint8_t key);
extern void keyboard_unset(uint8_t key);
