#include <stdio.h>
#include "keyboard.h"

uint8_t keyboard = K_NO_KEYS;

bool keyboard_get(uint8_t key) {
  return keyboard == key;
}

void keyboard_set(uint8_t key) {
  keyboard = key - 1;
}

void keyboard_unset(uint8_t key) {
  keyboard = K_NO_KEYS;
}
