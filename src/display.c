#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <SDL2/SDL.h>
#include "display.h"
#include "ram.h"

/* 
   64 - width of screen, 32 - height of screen 
*/
bool PIXELS[DISPLAY_WIDTH][DISPLAY_HEIGHT];

void initialize_display() {
  memset(PIXELS, 0, DISPLAY_WIDTH * DISPLAY_HEIGHT);
}


static SDL_Rect get_screen_rect(SDL_Window *window) {
  int SCREEN_WIDTH, SCREEN_HEIGHT;
  SDL_GetWindowSize(window, &SCREEN_WIDTH, &SCREEN_HEIGHT);
  SDL_Rect rect = {.x = (SCREEN_WIDTH - 512) / 2,
		   .y = (SCREEN_HEIGHT - 256) / 2,
		   .w = 512,
		   .h = 256};
  return rect;
}

SDL_Rect get_pixel_rect(SDL_Window *window, uint8_t x, uint8_t y) {
  SDL_Rect screen = get_screen_rect(window);
  SDL_Rect r = {.x = screen.x + (8 * x),
		.y = screen.y + (8 * y),
		.w = 8, .h = 8};
  return r;
}

int draw_screen(SDL_Window *window, SDL_Renderer *renderer) {
  SDL_SetRenderDrawColor(renderer, 100, 200, 100, 255);
  SDL_Rect screen = get_screen_rect(window);
  if (SDL_RenderFillRect(renderer, &screen) != 0) {
    return 1;
  }
  for (int x = 0; x < DISPLAY_WIDTH; x++)
    for (int y = 0; y < DISPLAY_HEIGHT; y++)
      if (get_pixel(x, y) == true)
	draw_pixel(window, renderer, x, y);

  return 0;
}

int draw_pixel(SDL_Window *window, SDL_Renderer *renderer, uint8_t x, uint8_t y) {
  SDL_Rect pixel = get_pixel_rect(window, x, y);
  SDL_SetRenderDrawColor(renderer, 100, 100, 200, 255);
  return SDL_RenderFillRect(renderer, &pixel);
}


bool draw_sprite_byte(uint8_t byte, uint8_t x, uint8_t y) {
  bool collision = false;
  for (uint8_t index = 0; index <= 7; index++) {
    if ( (byte & (1 << (7 - index))) != 0 ){
      if (get_pixel(x + index, y) == true) {
	remove_pixel(x + index, y);
	collision = true;
      } else insert_pixel(x + index, y);
    }
  }
  return collision;
}

bool draw_sprite(uint16_t start, uint8_t n,
		       uint8_t x, uint8_t y) {
  bool pixel_changed = false;
  bool collision = false;
  for (uint8_t index = 0; index < n; index++ ) {
    pixel_changed = draw_sprite_byte(ram[start + index], x, y++);
    if (collision == false) collision = pixel_changed;
  }
  return pixel_changed;
}


bool insert_pixel(uint8_t x, uint8_t y) {
  x = x % DISPLAY_WIDTH;
  y = y % DISPLAY_HEIGHT;
  return PIXELS[x][y] = true;
}

bool remove_pixel(uint8_t x, uint8_t y) {
  x = x % DISPLAY_WIDTH;
  y = y % DISPLAY_HEIGHT;
  return PIXELS[x][y] = false;
}

bool get_pixel(uint8_t x, uint8_t y) {
  x = x % DISPLAY_WIDTH;
  y = y % DISPLAY_HEIGHT;
  return PIXELS[x][y];
}
