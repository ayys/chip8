#include <stdint.h>

/* 
   Stack is an array of 16 16-bit values
   used to store the address that the interpreter
   should return to when finished with a
   subroutine.
 */
#define STACK_SIZE 16
extern uint16_t stack[];

extern void initialize_stack();
