#include "instructions.h"
#include "ram.h"

uint16_t ins_flip(uint16_t ins) {
  return ((ins_msbyte(ins) << 8) + ins_lsbyte(ins));
}

uint8_t ins_lsbyte(uint16_t ins) {
  return (ins >> 8) & 0x00ff;
}

uint8_t ins_msbyte(uint16_t ins) {
  return ins & 0x00ff;
}

uint8_t ins_mshex(uint16_t ins) {
  return (ins_msbyte(ins) >> 4) & 0x0f;
}

uint8_t ins_lshex(uint16_t ins) {
  return ins_lsbyte(ins) & 0x0f;
}
uint16_t ins_addr(uint16_t ins) {
  return ins_flip(ins) & 0x0fff;
}

uint8_t ins_hex(uint16_t ins, uint8_t pos){
  return (ins_flip(ins) >> 4*(3 - pos)) & 0x0f;
}

uint16_t ins_from_ram(uint16_t pc) {
  uint16_t ins = (ram[pc] << 8) + (ram[pc + 1]);
  return ins;
}
