#include <stdint.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#define INS_0NNN 0
#define INS_00E0 1
#define INS_00EE 2
#define INS_1NNN 3
#define INS_2NNN 4
#define INS_3XNN 5
#define INS_4XNN 6
#define INS_5XY0 7
#define INS_6XNN 8
#define INS_7XNN 9
#define INS_8XY0 10
#define INS_8XY1 11
#define INS_8XY2 12
#define INS_8XY3 13
#define INS_8XY4 14
#define INS_8XY5 15
#define INS_8XY6 16
#define INS_8XY7 17
#define INS_8XYE 18
#define INS_9XY0 19
#define INS_ANNN 20
#define INS_BNNN 21
#define INS_CXNN 22
#define INS_DXYN 23
#define INS_EX9E 24
#define INS_EXA1 25
#define INS_FX07 26
#define INS_FX0A 27
#define INS_FX15 28
#define INS_FX18 29
#define INS_FX1E 30
#define INS_FX29 31
#define INS_FX33 32
#define INS_FX55 33
#define INS_FX65 34

extern void _0nnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _00e0(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _00ee(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _1nnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _2nnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _3xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _4xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _5xy0(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _6xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _7xnn(SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy0 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy1 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy2 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy3 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy4 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy5 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy6 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xy7 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _8xye (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _9xy0 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _annn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _bnnn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _cxnn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _dxyn (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _ex9e (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _exa1 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx07 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx0a (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx15 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx18 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx1e (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx29 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx33 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx55 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);
extern void _fx65 (SDL_Window *window, SDL_Renderer *renderer, uint16_t ins);


extern void opcode_execute_at_loc(SDL_Window *window, SDL_Renderer *renderer, uint16_t pc);

extern bool pause_execution;
