#include <stdint.h>

/* 
   |---+---+---+---|
   | C | D | A | B |
   |---+---+---+---|

   A -> Most Significant Hex Value (mshex)
   D -> Least Significant Hex Value (lshex)
   AB -> Most Significant Byte (msbyte)
   CD -> Least Significant Byte (lsbyte)
   BCD -> Address (addr)
*/

#define ins_t uint16_t

/* 
   Get the LSByte of Instruction
 */
extern uint8_t ins_lsbyte(uint16_t ins);

/* 
   Get the MSByte of Instruction
 */
extern uint8_t ins_msbyte(uint16_t ins);

/* 
   Get the MSHex of Instruction
 */
extern uint8_t ins_mshex(uint16_t ins);

/* 
   Get the LSHex of Instruction
 */
extern uint8_t ins_lshex(uint16_t ins);

/* 
   Get the 12bit address stored in the instruction
 */
extern uint16_t ins_addr(uint16_t ins);

/* 
   Get hex at position pos
 */
extern uint8_t ins_hex(uint16_t ins, uint8_t pos);

/* 
   return an instruction from a location in ram
 */
extern uint16_t ins_from_ram(uint16_t pc);

extern uint16_t ins_flip(uint16_t ins);
