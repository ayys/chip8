/* 
   Main program in chip-8
*/

#include <stdio.h>

/* SDL2 dependencies */
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_image.h>



#include "ram.h"
#include "registers.h"
#include "stack.h"
#include "keyboard.h"
#include "display.h"
#include "opcodes.h"
#include "instructions.h"
#include "parse.h"

int main (int argc, char* argv[]) {
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Event event;
  initialize_ram();
  initialize_registers();
  initialize_stack();
  initialize_display();

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize SDL: %s", SDL_GetError());    
    return 1;
  }

  if (SDL_CreateWindowAndRenderer(320, 240, SDL_WINDOW_RESIZABLE, &window, &renderer)) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't create window and renderer: %s", SDL_GetError());
    return 1;
  }
  
  if (argc != 2) return 1;
  FILE *f = fopen(argv[1], "r");
  load_program_to_ram(f);

  #ifdef DEBUG
  for (int c = 512; c < program_length; c++) {
    printf("0x%x\t%x\n", c, ram[c]);
  }
  #endif
  
  while (pc < 4 * 1024){
    while( SDL_PollEvent( &event ) ){
      switch( event.type ){
      case SDL_QUIT:
	goto quit;
      case SDL_KEYDOWN:
	switch (event.key.keysym.sym) {
	case SDLK_0: keyboard_set(K_0); break;
	case SDLK_1: keyboard_set(K_1); break;
	case SDLK_2: keyboard_set(K_2); break;
	case SDLK_3: keyboard_set(K_3); break;
	case SDLK_4: keyboard_set(K_4); break;
	case SDLK_5: keyboard_set(K_5); break;
	case SDLK_6: keyboard_set(K_6); break;
	case SDLK_7: keyboard_set(K_7); break;
	case SDLK_8: keyboard_set(K_8); break;
	case SDLK_9: keyboard_set(K_9); break;
	case SDLK_a: keyboard_set(K_A); break;
	case SDLK_b: keyboard_set(K_B); break;
	case SDLK_c: keyboard_set(K_C); break;
	case SDLK_d: keyboard_set(K_D); break;
	case SDLK_e: keyboard_set(K_E); break;
	case SDLK_f: keyboard_set(K_F); break;
	}
        break;
      case SDL_KEYUP:
	switch (event.key.keysym.sym) {
	case SDLK_0: keyboard_unset(K_0); break;
	case SDLK_1: keyboard_unset(K_1); break;
	case SDLK_2: keyboard_unset(K_2); break;
	case SDLK_3: keyboard_unset(K_3); break;
	case SDLK_4: keyboard_unset(K_4); break;
	case SDLK_5: keyboard_unset(K_5); break;
	case SDLK_6: keyboard_unset(K_6); break;
	case SDLK_7: keyboard_unset(K_7); break;
	case SDLK_8: keyboard_unset(K_8); break;
	case SDLK_9: keyboard_unset(K_9); break;
	case SDLK_a: keyboard_unset(K_A); break;
	case SDLK_b: keyboard_unset(K_B); break;
	case SDLK_c: keyboard_unset(K_C); break;
	case SDLK_d: keyboard_unset(K_D); break;
	case SDLK_e: keyboard_unset(K_E); break;
	case SDLK_f: keyboard_unset(K_F); break;
	}
        break;
      default:
        break;
      }
    }

      if (pause_execution == false) {
	pc += 2;
	
      }
      opcode_execute_at_loc(window, renderer, pc);

      if (ram[pc])
      if (sp_rs.dt > 0) sp_rs.dt--;
      if (sp_rs.st > 0) sp_rs.st--;

      #ifdef DEBUG
      if (pause_execution == false && ram[pc + 1]){
	/* PRINT SYSTEM STATE */
	printf("INSTRUCTION : %x\n", ins_flip(ins_from_ram(pc)));
	/* registers */
	/* printf("REGISTERS\n-----------------\n"); */
	printf("I = %x\tPC = %x\tSP = %x\t", i, pc, sp);
	printf("DT = %x\tST = %x\n", sp_rs.dt, sp_rs.st);
	for ( int r = 0; r < 16; r++ ) printf("V%x = %x\t", r, gp_rs[r]);

	/* PRINT STACK */
	/* printf("STACK\n-----------------\n"); */
	/* for ( int r = 0; r < STACK_SIZE; r++ ) printf("%d\t", stack[r]); */
	/* printf("\n");printf("\n"); */
	/* ------------------ */
	for (int c = 512; c < 4*1024; c++) {
	  if (ram[c] != 0) printf("%x=%x\t",c, ram[c]);
	} printf("\n");
	printf("\n");printf("\n");	
      }
      #endif

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    draw_screen(window, renderer);

    SDL_RenderPresent(renderer);
    
    SDL_Delay(1000/60);		/* RUN AT 60Hz */
  }
 quit:
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
